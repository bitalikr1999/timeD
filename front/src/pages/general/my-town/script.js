import { mapGetters, mapActions, mapMutations } from 'vuex'

export default {
  computed: {
    ...mapGetters(['myTown'])
  },

  methods: {
    ...mapActions(['getTown']),
    ...mapMutations({
      setMyPlace: 'SET_MY_PLACE',
      setMyTown: 'SET_MY_TOWN'
    }),

    setPlaces () {
      this.setMyPlace(this.myTown.id)
      this.setMyTown(true)
    }
  },

  mounted () {
    
    if(this.myTown && this.myTown.image) {
      this.ea.$emit('endLoading')
    } else {
      this.getTown()
      .then(() => {
        this.setPlaces()
        this.ea.$emit('endLoading')
      })
    }
  }
}