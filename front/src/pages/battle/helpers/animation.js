const getGr = (elem1, elem2) => {
  const elem1cord = elem1.getBoundingClientRect()
  const elem2cord = elem2.getBoundingClientRect()
  let y = 0
  let x = (elem2.offsetTop + elem2.offsetHeight / 2) - (elem1.offsetTop + elem1.offsetHeight / 2)
  if (elem1.offsetLeft > elem2.offsetLeft) {
    y = (elem1.offsetLeft + elem1cord.width / 2) - (elem2.offsetLeft + elem2cord.width / 2)
  } else {
    y = (elem2.offsetLeft + elem2cord.width / 2) - (elem1.offsetLeft + elem1cord.width / 2)
  }
  let arctg = x / y
  let radiant = Math.atan(arctg)
  let gr = 90 - (radiant * 180 / Math.PI)
  let result = {}
  if (elem1.offsetLeft > elem2.offsetLeft) result.lefftetElem = 1
  else result.lefftetElem = 2

  if (elem1.offsetTop > elem2.offsetTop) result.topElem = 1
  else result.topElem = 2

  if (result.lefftetElem === 2) {
    result.gr = -gr
  } else result.gr = gr

  return result

}

const animationWithGr = (targetId, attackerId) => {
  const elem1 = document.getElementById('char' + targetId)
  const elem2 = document.getElementById('char' + attackerId)
  const grResult = getGr(elem1, elem2)
  return {
    elem1,
    elem2,
    grResult
  }
}

const animationAttack = (type, targetId, attacker) => {

  return new Promise((resolve, reject) => {

    const animationFunc = animationsFunc[type]

    if (!animationFunc) return resolve();

    const attackerId = attacker ? attacker.id : null

    animationFunc(targetId, attackerId)
    .then(() => resolve())
  })
}

const fireBall = (targetId, attackerId) => {
  return new Promise((resolve, reject) => {
    const {
      elem1,
      elem2,
      grResult
    } = animationWithGr(targetId, attackerId)

    const arrow = document.getElementById('fireball')
    const arrowCord = arrow.getBoundingClientRect()
    const gr = grResult.gr
    const startLeft = elem2.offsetLeft + 25 + 'px'
    arrow.style.transform = 'rotate('+gr+'deg)'
    arrow.style.left = startLeft
    arrow.style.top = elem2.offsetTop + 'px'
    arrow.classList.remove('pool')
    setTimeout(() => {
      arrow.style.opacity = 1
      arrow.style.transition = 'all 0.1s ease-in-out'
      setTimeout(() => {
        arrow.style.left = elem1.offsetLeft + ( (grResult.lefftetElem == 1) ? 0 : 50) + 'px'
        arrow.style.top = elem1.offsetTop + 65 + 'px'
        setTimeout(() => {
          arrow.style.opacity = 0
          arrow.style.transition = 'none'
          resolve()
        }, 150)
      }, 20)
    }, 10)

  })

}

const arrowAnimation = (targetId, attackerId) => {
  return new Promise((resolve, reject) => {
    const {
      elem1,
      elem2,
      grResult
    } = animationWithGr(targetId, attackerId)

    const arrow = document.getElementById('boow-arrow')
    const arrowCord = arrow.getBoundingClientRect()
    const gr = grResult.gr
    const startLeft = elem2.offsetLeft + 50 + 'px'
    arrow.style.transform = 'rotate('+gr+'deg)'
    arrow.style.left = startLeft
    arrow.style.top = elem2.offsetTop + 'px'
    arrow.classList.remove('pool')
    setTimeout(() => {
      arrow.style.opacity = 1
      arrow.style.transition = 'all 0.1s ease-in-out'
      setTimeout(() => {
        arrow.style.left = elem1.offsetLeft + ( (grResult.lefftetElem == 1) ? 0 : 100) + 'px'
        arrow.style.top = elem1.offsetTop + 60 + 'px'
        setTimeout(() => {
          arrow.classList.add('pool')
        }, 200)
        setTimeout(() => {
          arrow.style.opacity = 0
          arrow.style.transition = ''
          resolve()

        }, 250)
      }, 20)
    }, 10)

  })

}

const magicRound = async (targetId) => {
  const elem1 = document.getElementById('char' + targetId)
  const round = elem1.getElementsByClassName('magic-round')[0]
  round.classList.add('active')
  setTimeout(() => {
    round.classList.remove('active')
  }, 1500)
}


const animationsFunc = {
  'distance_rifle': arrowAnimation,
  'distance_magic': fireBall,
  'medication': fireBall,
  'magnification_yourself': magicRound
}


export {
  animationAttack
}
