import { mapGetters, mapActions, mapMutations } from 'vuex'
import { Carousel, Slide } from 'vue-carousel'

import winModal from './components/win'
import failModal from './components/fail'
import characterModal from './components/character'
import log from './components/log'
import animations from './components/animation'

import { animationAttack } from './helpers/animation'

export default {
  name: '',

  data () {
    return {
      show: true,

      myTeam: null,
      enemyTeam: null,
      activeChar: null,
      possibleAttack: null,
      socketWait: false,
      openModal: null,
      canAttack: true,
      hoverChar: null,
      logData: [],

      deleteBattle: false,

      rewardGold: null,
      rewardExpirience: null,
      myTurn: false,
      rewardItems: [],
      activeSkill: null
    }
  },

  sockets: {
    'battle/attack-sus' (result) {
      this.addLog({
        character: this.battleRound[0],
        text: `Нанес ${result.attackPoint} урона`
      })

      if (result.attackerTeamId == this.user.id) {
        this.socketAttack(result, 'enemyTeam')
      } else {
        this.socketAttack(result, 'myTeam')
      }
    },

    'battle/end' (result) {
      console.log('battle/end ', result)
    },

    'battle/win' (result) {
      this.openModal = 'win'
      this.rewardGold = result.gold
      this.rewardExpirience = result.exp
      this.rewardItems = result.data
      this.deleteBattle = true
    },

    'battle/loss' (result) {
      this.openModal = 'fail'
    },

    'battle/next-turn' () {
      const team = (this.battleRound[0].parentId == this.user.id) ? this.user : {}
      this.addLog({
        team,
        text: 'Пропустил ход'
      })
      this.skipRound()
    },

    'battle/not-you-turn' () {
      this.socketWait = true
    },

    'battle/move' (result) {
      this.addLog({
        character: this.battleRound[0],
        text: `Сменил позицию`
      })
      let team = (result.teamId == this.user.id) ? 'myTeam' : 'enemyTeam'
      this.moveCharacterVisual(team, result.moveTo)
    },

    'battle/heal' (result) {
      let team = (result.teamId == this.user.id) ? 'myTeam' : 'enemyTeam'
      this.addLog({
        character: this.battleRound[0],
        text: `Исцелил союзника на ${result.healPoint} жизней`
      })

      this.socketHeal (result, team)
    },

    'battle/skill-sus' (result) {
      if (result.error) {
        this.socketWait = false
        if (result.field == 'mana') {
          this.addLog({
            text: 'Недостаточно маны для использования умения'
          })
        }
        return
      }

      let text  = `Использовал умение '${result.skillName}'`
      if (result.attackPoint) text += ` и нанес ${result.attackPoint} урона`
      this.addLog({
        character: this.battleRound[0], text
      })

      if (result.isNotAttack) {
        this.socketWait = false
        this.skillUseRes(result)
        return;
      }

      if (result.attackerTeamId == this.user.id) {
        this.socketAttack(result, 'enemyTeam')
      } else {
        this.socketAttack(result, 'myTeam')
      }
    },

    'battle/buffs' (result) {
      let team = (result.teamId == this.user.id) ? 'myTeam' : 'enemyTeam'
      result.buffs.forEach(val => {
        this.addLog({
          character: (this.battleRound[0].id == result.characterId) ? this.battleRound[0] : '',
          text: 'Получил ' + val.attackPoint + ' от ефекта ' + val.buffName
        })
        setTimeout(()=>{
          this.updateHealth(val.attackPoint, result.characterId, team)
        }, 50)
      })
    }
  },

  components: {
    Carousel,
    Slide,
    winModal,
    failModal,
    characterModal,
    log,
    animations
  },

  computed: {
    ...mapGetters([
      'myTeamForBattle',
      'enemyTeamForBattle',
      'battleRound',
      'battleId',
      'user'
    ])
  },



  mounted () {
    this.myTeam = this.calcTeamsPosition(this.myTeamForBattle, true)
    this.enemyTeam = this.calcTeamsPosition(this.enemyTeamForBattle)

    this.getActiveChart()
    this.getPosibbleAttack()

    this.$once('hook:beforeDestroy',  () => {
      this.destroyBattle()
    })

    this.addLog({
      text: `Начало битвы`
    })
  },

  methods: {
    ...mapActions(['nextRoundItem', 'clearBattle']),
    ...mapMutations(['SET_BATTLE_ROUND']),

    addLog (data) {
      if (data.character) {
        const team = (data.character.parentId == this.user.id) ? this.myTeam : this.enemyTeam
        Object.keys(team).forEach(key => {
          if (team[key] && team[key].id == data.character.id) data.character = team[key]
        })
        data.team = (data.character.parentId == this.user.id) ? this.user : {}
      }
      data.date = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds()
      this.logData.push(data)
    },

    socketAttack(result, team) {
      if(result.newRound) {
        this.SET_BATTLE_ROUND(result.newRound)
      } else if (result.nextRound === false) {} else {
        this.nextRoundItem()
      }
      this.sound(this.activeChar.character_type_slug)
      animationAttack(this.activeChar.character_type_slug, result.targetId, this.activeChar)
      .then(() => {
        this.updateHealth(result.attackPoint, result.targetId, team)
        this.manaChange(result.mana, this.activeChar.id, team)
        this.getActiveChart()
        this.getPosibbleAttack()
        this.socketWait = false
      })
    },

    socketHeal (result, team) {
      this.nextRoundItem()
      this.addHealth(result.healPoint, result.targetId, team)
      this.getActiveChart()
      this.getPosibbleAttack()
    },

    manaChange (mana, targetId, team) {
      team = (team == 'enemyTeam') ? 'myTeam' : 'enemyTeam'
      const newTeam = this[team]
      Object.keys(newTeam).forEach(key => {
        if (newTeam[key] && newTeam[key].id == targetId) newTeam[key].mana = mana
      })
      this[team] = newTeam
    },

    getTurn () {
      let id  = this.battleRound[0].id
      let result = false
      Object.keys(this.myTeam).forEach(key => {
        const val = this.myTeam[key]
        if (val && val.id == id) {
          result = true
        }
      })
      this.myTurn = result
    },

    calcTeamsPosition (val, reverse) {
      let elems = {}
      let elemWitPosition = {}

      if (!val.forEach) {
        val = this.mixArray(val)
      }

      val.forEach(element => {
        if(element.position == null) {
          this.emptys--
        }
        else {
          elemWitPosition[element.position] = element
        }
      })

      if(reverse) {
        for (let index = 1; index <= 10; index++) {
          elems[index] = elemWitPosition[index] || null
        }
      } else {
        for (let index = 10; index >= 1; index--) {
          elems[11 - index] = elemWitPosition[index] || null
        }
      }

      return elems
    },

    mixArray (obj) {
      let array = []
      Object.keys(obj).forEach(key => {
        if(obj[key]) array.push(obj[key])
      })
      return array
    },

    getActiveChart () {
      for (let index = 1; index <= 10; index++) {
        const element = this.myTeam[index]
        if(element && element.id == this.battleRound[0].id) {
          this.activeChar = element
          break
        } else {
          let element2 = this.enemyTeam[index]
          if(element2 && element2.id == this.battleRound[0].id) {
            this.activeChar = element2
            break
          }
        }
      }
      this.getTurn()

    },

    getPosibbleAttack () {
      if (this.battleRound[0].parentId != this.user.id) {
        this.possibleAttack = []
        return;
      }

      this.possibleAttack = []
      if (this.getTypeCodePosition(this.activeChar) > 1) {
        for (let index = 1; index <= 10; index++) {
          const element =  this.enemyTeam[index]
          element && this.possibleAttack.push(element.id)
        }
      } else if(this.activeChar.position < 6){
        for (let index = 1; index <= 10; index++) {
          const element =  this.enemyTeam[index]
          element && element.position < 6 && this.possibleAttack.push(element.id)
        }

        if(this.possibleAttack.length < 1) {
          for (let index = 1; index <= 10; index++) {
            const element =  this.enemyTeam[index]
            element && this.possibleAttack.push(element.id)
          }
        }
      }
    },

    getTypeCodePosition (target) {
      switch (target.character_type_slug) {
        case 'melee':
          return 1;
        break;
        case 'distance_rifle':
        case 'distance_magic':
          return 2
        break;
        case 'medication':
          return 3;
        break;
      }
    },

    attackThis (character) {
      if (this.canAttack) {
        if (!this.activeSkill) {
          this.canAttack = false
          setTimeout( () => {
            this.canAttack = true
            if (this.socketWait) return false
            this.socketWait = true
            this.$socket.emit('battle/attack', {
              targetId: character.id
            })
          }, 300)
        } else {
          this.canAttack = false
          setTimeout( () => {
            this.canAttack = true
            if (this.socketWait) return false
            this.socketWait = true
            this.$socket.emit('battle/use-skill', {
              targetId: character.id,
              skillId: this.activeSkill.id
            })
            this.activeSkill = null
          }, 300)
        }
      }
    },

    healThis (character) {
      if (this.canAttack) {
        this.canAttack = false
        setTimeout( () => {
          this.canAttack = true
          if (this.socketWait) return false
          this.$socket.emit('battle/heal', {
            targetId: character.id
          })
        }, 300)
      }
    },

    updateHealth (damage, id, team) {
      let array = JSON.parse(JSON.stringify(this[team]))
      let i = 0

      Object.keys(array).forEach(key => {
        const val = array[key]
        if(val && val.id == id) {
          let elem = document.getElementById('char' + val.id)
          elem.classList.add('damage')
          setTimeout(() => {
            elem.classList.remove('damage')
          }, 100)
          if (damage === -1)
            array[key].health = 0
          else
            array[key].health = array[key].health - damage

          if (array[key].health < 1) {
            delete array[key]
            let reverse = false
            if (team == 'myTeam') reverse = true
            array = this.calcTeamsPosition(array, reverse)
          }
        }
        i++

      })
      this[team] = array
    },

    addHealth (heal, id, team) {
      let array = JSON.parse(JSON.stringify(this[team]))
      let i = 0

      Object.keys(array).forEach(key => {
        const val = array[key]
        if(val && val.id == id) {
          let elem = document.getElementById('char' + val.id)
          elem.classList.add('heal')
          setTimeout(() => {
            elem.classList.remove('heal')
          }, 100)
          array[key].health = array[key].health + heal
        }
        i++

      })
      this[team] = array
    },

    moveCharacter (to) {
      if (this.activeChar.parentId != this.user.id) return false
      this.socketWait = true
      this.moveTo = to
      this.$socket.emit('battle/move', {charcterId: this.activeChar.id, moveTo: to})
    },

    moveCharacterVisual (team, moveTo) {
      let oldPosition
      let canonicalPosition = moveTo
      if (team == 'enemyTeam') {
        oldPosition = 10 - this.activeChar.position + 1
        moveTo = 10 - moveTo + 1
      } else {
        oldPosition = this.activeChar.position
      }
      this.socketWait = false
      let char = null
      if(this[team][moveTo]) {
        char = this[team][moveTo]
      }
      this[team][oldPosition] = char
      this[team][moveTo] = this.activeChar
      this.activeChar.position = Number(canonicalPosition)

      this.nextRoundItem()
      this.getActiveChart()
      this.getPosibbleAttack()
    },

    pressSkill (data) {
      // if (!skill || skill.id) return;
      this.activeSkill = data.skill
      // this.$socket.emit('battle/use-skill', {
      //   charcterId: this.activeChar.id,
      //   target
      //   skillId: skill.id
      // })
    },

    skillUseRes (result) {
      this.sound(result.skillSlug)
      animationAttack(result.skillSlug, result.targetId)
    },

    finishBattle () {
      this.show = false
      this.clearBattle()
      this.$router.go(-1)
    },

    finishBattleFail () {
      this.destroyBattle()

    },

    destroyBattle () {
      if (!this.deleteBattle)
      this.$http.delete('/api/battles')
    },

    skipRound () {
      this.nextRoundItem()
      this.getActiveChart()
      this.getPosibbleAttack()
    },

    sound (typeAttack) {
      switch (typeAttack) {
        case 'melee' : document.getElementById('attack').play(); break;
        case 'distance_rifle' : document.getElementById('arrow').play(); break;
        case 'medication' : document.getElementById('fireAttack').play(); break;
        case 'magnification_yourself': document.getElementById('magicRoundMus').play(); break;
      }
    },
  }
}
