import { mapGetters, mapActions, mapMutations } from 'vuex'
import heroInfo from './hero-info'

export default {
  data () {
    return {
      isOpen: 'menu',
      login: null,
      password: null,
      registerData: {
        login: null,
        email: null,
        password: null,
        name: null,
        heroSlug: null
      },
      characters: [],
      chooseHeros: false,
      choosenHero: null
    }
  },

  components: {
    heroInfo
  },

  sockets: {
    'auth-susseful' (data) {
      this.SET_CONNECTION(data.id)
      this.loadDataAfter()
    }
  },

  methods: {
    ...mapMutations([
      'SET_CONNECTION'
    ]),
    ...mapActions(['getUser', 'getTown']),
    signIn () {
      this.ea.$emit('startLoading')
      this.$http.post('/auth', {login: this.login, password: this.password})
      .then(this.signSus)
      .catch(this.signFail)
    },

    signSus (result) {
      localStorage.setItem('__TOKEN__', result.data)
      this.ea.$emit('close-modal', 'auth')
      this.$socket.emit('auth', {token: localStorage.getItem('__TOKEN__')})
    },

    loadDataAfter () {
      let getUser = this.getUser()
      Promise.all([getUser])
      .then(() => {
        this.$router.push('/my-town')
      })
      .catch((error) => {
        localStorage.removeItem('__TOKEN__')
      })
    },

    signFail (error) {
      this.ea.$emit('endLoading')
    },

    signUp () {
      if (!this.choosenHero) return;
      this.registerData.heroSlug = this.choosenHero.slug
      this.ea.$emit('startLoading')
      this.$http.put('/auth', this.registerData)
      .then(this.signSus)
      .catch(this.signFail)
    },

  },

  mounted () {
    this.$http.get('/auth/heros')
    .then(result => {
      this.characters = result.data
      console.log(result)
    })
  }
}
