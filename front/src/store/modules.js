import user from './modules/user'
import myTown from './modules/my-town'
import characters from './modules/characters'
import base from './modules/base'
import battle from './modules/battle'
import items from './modules/items'
import recipes from './modules/recipes'

export default {
  user,
  myTown,
  characters,
  base,
  battle,
  items,
  recipes
}
