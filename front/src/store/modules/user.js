import $http from '@/utils/http.js'

const state = {
  user: null
}

// getters
const getters = {
  user: state => state.user
}

// actions
const actions = {
  getUser ({commit, state}) {
    return new Promise((resolve, reject) => {
      $http.get('/user').then(
      (response) => {
        commit('SET_USER', response.data)
        resolve(true)
      }, (error) => {
        reject(error)
      })
    })
  }
}

// mutations
const mutations = {
  SET_USER (state, user) {
    state.user = user
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
