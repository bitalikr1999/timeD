import $http from '@/utils/http.js'

const state = {
  myTeam: null,
  enemyTeam: false,
  round: null,
  battleId: null
}

// getters
const getters = {
  myTeamForBattle: state => state.myTeam,
  enemyTeamForBattle: state => state.enemyTeam,
  battleRound: state => state.round,
  battleId: state => state.battleId
}

// actions
const actions = {
  nextRoundItem ({commit, state}) {
    let array = JSON.parse(JSON.stringify(state.round))

    let item = array.shift()
    array.push(item)

    commit('SET_BATTLE_ROUND', array)
    
  },

  updateHealthEnemy ({commit, state}, data) {
    let damage = data.damage
    let id = data.id

    let array = JSON.parse(JSON.stringify(state.enemyTeam))

    array.forEach((val, i) => {
      if(val.id == id) 
        array[i].health -= damage
    })
    commit('SET_MY_ENEMY_FOR_BATTLE', array)
  },

  clearBattle ({commit, state}) {
    commit('SET_MY_TEAM_FOR_BATTLE', null)
    commit('SET_MY_ENEMY_FOR_BATTLE', null)
    commit('SET_BATTLE_ROUND', null)
    commit('SET_BATTLE_ID', null)
  }
}

// mutations
const mutations = {
  SET_MY_TEAM_FOR_BATTLE (state, myTeam) {
    state.myTeam = myTeam
  },

  SET_MY_ENEMY_FOR_BATTLE (state, enemyTeam) {
    state.enemyTeam = enemyTeam
  },

  SET_BATTLE_ROUND (state, round) {
    state.round = round
  },

  SET_BATTLE_ID(state, battleId) {
    state.battleId = battleId
  }

}

export default {
  state,
  getters,
  actions,
  mutations
}
