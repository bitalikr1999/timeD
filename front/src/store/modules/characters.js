import $http from '@/utils/http.js'

const state = {
  myTeam: null
}

// getters
const getters = {
  myTeam: state => state.myTeam
}

// actions
const actions = {
  getMyTeam ({commit, state}) {
    return new Promise((resolve, reject) => {
      $http.get('/team').then(
      (response) => {
        commit('SET_MY_TEAM', response.data)
        resolve(true)
      }, (error) => {
        reject(error)
      })
    })
  }
}

// mutations
const mutations = {
  SET_MY_TEAM (state, team) {
    state.myTeam = team
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
