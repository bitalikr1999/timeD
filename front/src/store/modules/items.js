import $http from '@/utils/http.js'

const state = {
  items: null
}

// getters
const getters = {
  items: state => state.items,
  
  itemsObj: state => {
    if (!state.items) return false
    const prepareUserItems = {}
    Object.keys(state.items).forEach(key => {
      prepareUserItems[state.items[key].item_id] = state.items[key]
    })
    return prepareUserItems
  },

  callCards: state => {
    const result = []
    if (!state.items) return false
    Object.keys(state.items).forEach(key => {
      if (state.items[key].type_slug == 'character-card') result.push(state.items[key])
    })
    return result
  },

  shereItems: state => {
    const result = []
    if (!state.items) return false
    Object.keys(state.items).forEach(key => {
      if (state.items[key].type_slug == 'city_sphere') result.push(state.items[key])
    })
    return result
  }
}

// actions
const actions = {
  getItems ({commit, state}) {
    return new Promise((resolve, reject) => {
      $http.get('/items').then(
      (response) => {
        commit('SET_ITEMS', response.data)
        resolve(response.data)
      }, (error) => {
        reject(error)
      })
    })
  }
}

// mutations
const mutations = {
  SET_ITEMS (state, items) {
    state.items = items
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
