import $http from '@/utils/http.js'

const state = {
  recipes: null
}

// getters
const getters = {
  recipes: state => state.recipes,
}

// actions
const actions = {
  async getRecipes ({ commit, state }) {
    const response = await $http.get('/craft')

    Object.keys(response.data).forEach(key => {
      response.data[key].forEach((val, i) => {
        response.data[key][i].resultSus = false
        response.data[key][i].resultError = false
        response.data[key][i].isCraft = null
        response.data[key][i].canCraft = (recipe, itemsObj) => {
          let result = true
          recipe.items.forEach(val => {
            const userItem = itemsObj[val.item_id]
            if (!userItem) result = false
            else if (userItem.count < val.count) result = false
          })
          recipe.isCraft = result
        }
      })
    })

    commit('SET_RECIPIES', response.data)
  },

  canCraft ({ commit, state }, data) {
    let { recipes, userItems } = data
    // recipes = JSON.parse(JSON.stringify(recipes))
    const result = {}
    Object.keys(recipes).forEach(key => {

      const cat = recipes[key]
      cat.forEach((val, i) => {

        let valResult = true
        val.items.forEach(val => {

          const userItem = userItems[val.item_id]
          if (!userItem) valResult = false
          else if (userItem.count < val.count) valResult = false

        })
        cat[i].isCraft = valResult

      })

      result[key] = cat
    })

    commit('SET_RECIPIES', result)

  }
}

// mutations
const mutations = {
  SET_RECIPIES (state, recipes) {
    state.recipes = recipes
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
