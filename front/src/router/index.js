import Vue from 'vue'
import Router from 'vue-router'

import commonHome from '@/pages/common/home'

import generalLayout from '@/pages/general'
import myTown from '@/pages/general/my-town'

import battle from '@/pages/battle'

Vue.use(Router)

export default new Router({
  // hashbang: false,
  // history: true,
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: commonHome
    },

    {
      path: '',
      name: 'General',
      component: generalLayout,
      children: [
        {
          path: '/my-town',
          name: 'MyTown',
          component: myTown
        }
      ]
    },

    {
      path: '/battle/:battleId',
      name: 'battle',
      component: battle
    }
  ]
})
