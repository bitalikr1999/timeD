import decor from '@/components/modal-decor'
import { mapGetters } from 'vuex'

export default {
  name: '',

  components: {decor},

  data () {
    return {
      levels: []
    }
  },

  methods: {
    getLevels () {
      this.$http.get('/campains')
      .then(result => {
        this.levels = result.data
      })
    },

    attack (level) {
      this.$http.put('/campains', {
        levelId: level.id
      })
      .then(result => {
        console.log(result)
      })
    }
  },

  mounted () {
    this.getLevels()
  }
}
