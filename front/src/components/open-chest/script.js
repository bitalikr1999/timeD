import decor from '@/components/modal-decor'
import { mapGetters, mapActions } from 'vuex'

export default {
  name: '',

  components: {decor},

  data () {
    return {
      bagItems: [],
      open: false,
      chest: {},
      results: []
    }
  },

  computed: {
    ...mapGetters(['user'])
  },

  methods: {
    ...mapActions(['getItems']),

    openChest (item) {
      this.$http.post('/items', {
        itemId: this.chest.id
      })
      .then(result => {
        this.results = result.data
        this.getItems()
      })
    },
  },

  mounted() {
    this.ea.$on('open-chest', data => {
      this.chest = data.item
    })

    this.$once('hook:beforeDestroy',  () => {
      // this.ea.$off('open-Bag')
    })
  },
}
