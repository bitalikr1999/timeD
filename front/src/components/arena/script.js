import decor from '@/components/modal-decor'
import { mapGetters } from 'vuex'

export default {
  name: '',

  components: {decor},

  data () {
    return {
      load: false,
      myLogs: null,
      top: null,
    }
  },

  computed: {
    ...mapGetters(['user'])
  },

  methods: {
    startBattle() {
      this.load = true
      this.$http.put('/arena/')
    },

    stop() {
      this.load = false
      this.$http.delete('/arena/')
    },

    getMyLogs () {
      this.$http.get('/arena/my-logs')
      .then(result => {
        this.myLogs = this.prepareLogs(result.data)
      })
    },

    getTop () {
      this.$http.get('/arena/top')
      .then(result => {
        this.top = result.data
      })
    },

    prepareLogs (logs) {
      const result = []
      logs.forEach(val => {
        const isWin = (val.user_win_id == this.user.id)
        result.push({
          win: isWin,
          opponentName: isWin ? val.user_loss_name : val.user_win_name,
          rate: val.rate
        })
      })
      return result
    }
  },

  mounted() {
    this.ea.$on('open-Arena', () => {
      this.getMyLogs()
      this.getTop()
    })

    this.$once('hook:beforeDestroy',  () => {
      this.ea.$off('open-Arena')
    })
  },
}
