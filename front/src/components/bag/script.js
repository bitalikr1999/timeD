import decor from '@/components/modal-decor'
import { mapGetters, mapActions } from 'vuex'

export default {
  name: '',

  components: {decor},

  data () {
    return {
      open: false,
      dropdown: false
    }
  },

  computed: {
    ...mapGetters(['user', 'items'])
  },

  methods: {
    ...mapActions(['getItems']),

    onDragged({ el, deltaX, deltaY, offsetX, offsetY, clientX, clientY, first, last }) {
        if (first) {
          this.dragged = true
          return
        }
        if (last) {
          this.dragged = false
          return
        }
        var l = +window.getComputedStyle(el)['left'].slice(0, -2) || 0
        var t = +window.getComputedStyle(el)['top'].slice(0, -2) || 0
        el.style.left = l + deltaX + 'px'
        el.style.top = t + deltaY + 'px'
    },

    showDropdow (item) {
      if (!item) return
      item.show = !item.show
    },

    useItem (item) {
      this.$http.post('/items', {
        itemId: item.id
      })
      .then(result => {
        this.updateItems()
      })
    },

    deleteItem (item) {
      this.$http.delete('/items', {
        itemId: item.id
      })
      .then(result => {
        this.updateItems()
      })
    },

    updateItems () {
      this.getItems()
    },

    openChest (item) {
      this.ea.$emit('open-chest', {
        item: item
      })
    }
  },

  mounted() {

    this.ea.$on('open-bag', () => {
      this.updateItems()
      this.open = true
    })

    this.$once('hook:beforeDestroy',  () => {
      this.ea.$off('open-Bag')
    })
  },
}
