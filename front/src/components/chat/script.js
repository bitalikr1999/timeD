module.exports = {
  name: 'chat',

  sockets: {
    'chat/message' (result) {
      const elem = document.getElementById('chats-message')

      this.messages[result.chat].push({
        date: new Date().getHours() + ':' + new Date().getMinutes(),
        message: result.message,
        user: result.user
      })
      setTimeout(() => {
        elem.scrollTop = elem.scrollHeight ;
      }, 50)
    }
  },

  data () {
    return {
      message: null,
      chat: 'world',
      messages: {
        world: []
      }
    }
  },

  methods: {
    send() {
      this.$socket.emit('chat/send', {
        message: this.message,
        chat: this.chat
      })
      this.message = null
    }
  }
}
