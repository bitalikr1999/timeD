import { mapGetters, mapActions, mapMutations} from 'vuex'

export default {
  name: "",

  data () {
    return {
      open: false,
      team: null
    }
  },

  computed: {
    ...mapGetters(['myPlace', 'isMyTown', 'user']),
  },

  methods: {
    ...mapMutations([
      'SET_MY_TEAM_FOR_BATTLE',
      'SET_MY_ENEMY_FOR_BATTLE',
      'SET_BATTLE_ROUND',
      'SET_BATTLE_ID'
    ]),
    getDistrictTeam () {
      if(this.isMyTown) {
        this.getDistrictTeamQuery(true)
      } else {
        this.getDistrictTeamQuery(false, this.myPlace)
      }
    },

    getDistrictTeamQuery (belonging, townId) {
      let url = belonging ? '/district-enemy' : `/district-enemy/${townId}`
      this.$http.get(url)
      .then((result) => {
        this.open = true
        this.team = result.data
      })
    },

    attack () {
      this.$socket.emit('battle/new', {
        oponentId: this.team.id,
        oponentType: 'enemy'
      })
    },

    getout () {
      this.$http.delete('/district-enemy/get-out')
      .then(() => {
        this.open = false
      })
    }
  },

  mounted () {
    this.ea.$on('endLoading', () => {
      this.getDistrictTeam()
    })
  }
}
