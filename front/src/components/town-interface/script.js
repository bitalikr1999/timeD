import decor from '@/components/modal-decor'
import { mapGetters, mapActions } from 'vuex'

export default {
  name: 'town-interface',

  components: {decor},

  data () {
    return {
      chosenTab: 'info',

      error: {
        mine: null,
        town: null
      },

      choosenSphere: null,
      shereTown: null
    }
  },

  computed: {
    ...mapGetters(['myTown', 'user', 'shereItems'])
  },

  methods: {
    ...mapActions(['getTown']),
    upgradeMine () {
      this.$http.put('/api/town/my-town/upgrade-mine')
      .then((result) => {
        this.getTown()
      })
      .catch((error) => {

      })
    },

    chooseSphere (sphere) {
      this.choosenSphere = sphere
      this.$http.post('/items/town-shere', {
        sphereId: sphere.id
      })
      .then(result => {
        this.shereTown = result.data
      })
    },

    openPortal () {
      this.$http.post('/items/', {
        itemId: this.choosenSphere.id
      })
    },

    tab (name) {
      this.chosenTab = name
    },

    upgradeTown () {
      this.$http.put('/api/town/my-town/upgrade-town')
      .then((result) => {

      })
    },

    calc (number, rounds) {
      let result = number
      for (let index = 0; index < rounds; index++) {
        result = result * 2
      }
      return result
    }
  }
}
