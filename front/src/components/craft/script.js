import decor from '@/components/modal-decor'
import { mapGetters, mapActions } from 'vuex'
import async from 'async'
const sleep = m => new Promise(r => setTimeout(r, m))

export default {
  name: '',

  components: {decor},

  data () {
    return {
      choosenType: 'resources',
      craftLoad: false,
      craftPersent: 0,
      userItems: [],
      inputCount: 0
    }
  },

  computed: {
    ...mapGetters(['recipes', 'user', 'itemsObj']),
  },

  methods: {
    ...mapActions(['getRecipes', 'canCraft', 'getItems']),

    async craftItem (recipe) {
      const funcs = []
      let next = true

      for (var i = 0; i < recipe.inputCount; i++) {
        funcs.push(async(callback) => {
          if (!next) return callback()
          if (!(await this.craftItemOne(recipe))) next = false
          await sleep(500)
          callback()
        })
      }

      async.waterfall(funcs, () => {
        recipe.inputCount = 1
      })
    },

    async craftItemOne (recipe) {
      try {
        await this.$http.post('/craft', {
          recipeId: recipe.id,
          count: recipe.inputCount ? Number(recipe.inputCount) : 1
        })
        recipe.resultSus = true
        await sleep(600)
        recipe.resultSus = false
        await this.updateC()
        return true;
      } catch (e) {
        recipe.resultError = true
        await sleep(600)
        recipe.resultError = false
        await this.updateC()
        return false
      }
    },

    async updateC () {
      await this.getItems()
      this.canCraft({
        recipes: this.recipes,
        userItems: this.itemsObj
      })
      return ;
    }

  },

  async mounted () {
    if (!this.itemsObj) {
      await this.getItems()
    }
    if (!this.recipes) {
      await this.getRecipes()
      await this.canCraft({
        recipes: this.recipes,
        userItems: this.itemsObj
      })
    }
  }
}
