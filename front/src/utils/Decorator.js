let single = new class Decorator {
  constructor () {

  }

  decorByProp (objects, prop) {
    if (!objects[0] || !objects[0][prop]) return false
    let result = {}
    objects.forEach(element => {
      if (!result[element[prop]]) 
        result[element[prop]] = []
        
      result[element[prop]].push(element)
    })
    return result
  }
}

export default single