import axios from 'axios'
import {API_URL} from '@/config.json'

const $http = axios.create({
  baseURL: API_URL,
  timeout: 1000,
  crossDomain: true,
  headers: {
    'Access-Control-Allow-Credentials': true
  }
})

$http.interceptors.request.use(function (config) {
  let token = localStorage.getItem('__TOKEN__')

  if (token != null) config.headers['Authorization'] = 'Bearer ' + token
  return config
})

export default $http
//https://timeofdiscort.herokuapp.com